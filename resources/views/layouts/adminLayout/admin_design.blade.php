<!DOCTYPE html>
<html dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.adminLayout.partials._head')

<body>

@include('layouts.adminLayout.partials._preloader')

<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <div id="app">
    @include('layouts.adminLayout.partials._header')

    @include('layouts.adminLayout.partials._sidebar')

    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        {{--@include('layouts.adminLayout.partials._breadcrumbs')--}}

        @yield('content')

        @include('layouts.adminLayout.partials._footer')
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

@include('layouts.adminLayout.partials._bottom_javascript')
</body>

</html>