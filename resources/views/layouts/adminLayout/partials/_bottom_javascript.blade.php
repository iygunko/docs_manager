

@if(Request::is('admin/article/create') || Request::is('admin/article/*/edit'))
<script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@endif
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('assets/extra-libs/sparkline/sparkline.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('js/backend_js/waves.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('js/backend_js/sidebarmenu.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('js/backend_js/custom.min.js') }}"></script>
<!--This page JavaScript -->
<!-- <script src="/dist/js/pages/dashboards/dashboard1.js"></script> -->
<!-- Charts js Files -->
<script src="{{ asset('assets/libs/flot/excanvas.js') }}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.time.js') }}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('assets/libs/flot/jquery.flot.crosshair.js') }}"></script>
<script src="{{ asset('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('js/backend_js/pages/chart/chart-page-init.js') }}"></script>



@yield('bottom_js')
