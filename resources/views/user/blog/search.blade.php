@extends('layouts.adminLayout.admin_design')

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title')  Search: {{$q}} @endslot
        @slot('parent') Homepage @endslot
        @slot('active') Search @endslot
    @endcomponent

        <div class="container">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12 mx-auto">
                        @if(isset($error))
                            <div class="alert alert-danger alert-block">
                                {!! $error !!}
                            </div>
                        @endif
                        <div class="card">
                            @if(isset($articles))
                            <div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Сategories</th>
                                        <th scope="col">Viewed</th>
                                        <th scope="col">Author</th>
                                        <th scope="col">Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($articles as $article)
                                        <tr>
                                            <td><a href="{{ route('article', $article->slug) }}">{{ $article->title }}</a></td>
                                            <td>{!! $article->description_short !!}</td>
                                            <td>
                                                @foreach($article->categories as $category)
                                                    {{$category->title}} <br>
                                                @endforeach
                                            </td>
                                            <td>{{ $article->viewed }}</td>
                                            <td>{{ $article->author }}</td>
                                            <td>{{ date('d.m.Y', strtotime($article->updated_at)) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @if(isset($articles))
                    {{ $articles->appends(Request::except('page'))->links() }}
                @endif

        </div>
    </div>
@endsection