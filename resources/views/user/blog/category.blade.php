@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Category')

@section('title', $category->title)

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title') Category @endslot
        @slot('parent') Homepage @endslot
        @slot('active') {{$category->title}} @endslot
    @endcomponent

    <div class="container">
       @forelse ($articles as $article)
           <div class="row">
               <div class="col-sm-12">
                   <h2><a href="{{route('article', $article->slug)}}">{!! $article->title !!}</a></h2>
                   <p>
                       {!! $article->description_short !!}
                   </p>
               </div>
           </div>
       @empty
            <h2 class="text-center">No records at the moment</h2>
        @endforelse
        {{ $articles->links() }}

    </div>
@endsection