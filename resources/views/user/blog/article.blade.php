@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Article')

@section('title', $article->title)
@section('meta_keyword', $article->meta_title)
@section('meta_description', $article->meta_description)

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title')  {{$article->title}} @endslot
        @slot('parent') Homepage @endslot
        @slot('active') {{$article->title}} @endslot
    @endcomponent

    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1>{{$article->title}}</h1>
                    <hr/>
                    <div class="text-right">
                        Автор: {!! $created_by !!} Дата изменения: {!! date('d.m.Y',strtotime($article->updated_at)) !!} Просмотров: {!! $article->viewed !!}
                    </div>
                    <div class="text-left">{!! $article->description !!}</div>
                </div>
            </div>
        </div>
    </div>
@endsection