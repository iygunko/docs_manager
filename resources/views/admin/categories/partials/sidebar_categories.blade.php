@if(isset($sidebarCategories) && count($sidebarCategories) > 0)
    @foreach($sidebarCategories as $sidebarCategory)
        {{----}}
        {{--@if(isset($sidebarCategory->children) && $sidebarCategory->children->count() > 0)--}}
        {{--@include('admin.categories.partials.sidebar_categories', ['sidebarCategories' => $sidebarCat<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">{{ $sidebarCategory->name }}</span></a>--}}
        {{--</li>egories->children])--}}
        {{--@endif--}}

        @if(!isset($subcategory) && $sidebarCategory->parent_id == 0 || isset($subcategory))

            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">{{ $sidebarCategory->title }}</span></a>
                @if((isset($sidebarCategory->articles) && count($sidebarCategory->articles) > 0) || (isset($sidebarCategory->children) && count($sidebarCategory->children) > 0) )
                    <ul aria-expanded="false" class="collapse  first-level">
                        @if(isset($sidebarCategory->children) && count($sidebarCategory->children) > 0)
                            @include('admin.categories.partials.sidebar_categories', [
                            'sidebarCategories' => $sidebarCategory->children,
                            'subcategory' => true,
                            ])
                        @endif
                        @if((isset($sidebarCategory->articles) && count($sidebarCategory->articles) > 0))
                            @foreach($sidebarCategory->articles as $categoryArticle)
                                <li class="sidebar-item"><a href="{{ route('article', $categoryArticle->slug) }}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">{{ $categoryArticle->title }}</span></a></li>
                            @endforeach
                        @endif
                    </ul>
                @endif
            </li>
        @endif
    @endforeach
@endif