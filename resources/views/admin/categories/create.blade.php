@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Create Category')

@section('title', 'Create Category')

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title') Category create @endslot
        @slot('parent') Homepage @endslot
        @slot('active') Category create @endslot
    @endcomponent

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <form class="form-horizontal" action="{{ route('admin.category.store') }}" method="post">
                    {{ csrf_field() }}
                    @include('admin.categories.partials.form')
                </form>
            </div>
        </div>
    </div>


@endsection