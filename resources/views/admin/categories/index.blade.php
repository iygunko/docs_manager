@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Categories')

@section('title', 'Categories')

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title') Categories list @endslot
        @slot('parent') Homepage @endslot
        @slot('active') Categories @endslot
    @endcomponent
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <a href="{{ route('admin.category.create') }}" class="btn btn-primary pull-right">
                    <i class="fas fa-plus"></i> Create category</a>
                <table class="table table-striped">
                    <thead>
                    <th>Name</th>
                    <th>Publishing</th>
                    <th class="text-right">Actions</th>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <td>{{ $category->title  }}</td>
                            <td>{{ $category->published  }}</td>
                            <td class="text-right">
                                <form onsubmit="if(confirm('Delete?')){ return true }else{ return false }" action="{{route('admin.category.destroy', $category)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}

                                    <a class="btn" href="{{ route('admin.category.edit', $category) }}">
                                        <i class="fa fa-edit"></i></a>

                                    <button type="submit" class="btn"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="text-center"><h2>Data not found</h2></td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3">
                            <ul class="pagination pull-right">
                                {{ $categories->links() }}
                            </ul>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>


@endsection