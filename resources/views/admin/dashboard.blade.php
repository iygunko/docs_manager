@extends('layouts.adminLayout.admin_design')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <div class="jumbotron jumbotron-fluid text-sm-center bg-dark">
                    <p><span class="label label-primary">Total Categories {{$count_categories}}</span></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron jumbotron-fluid text-sm-center bg-dark">
                    <p><span class="label label-primary">Total Articles {{$count_articles}}</span></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron jumbotron-fluid text-sm-center bg-dark">
                    <p><span class="label label-primary">Total users {{$count_users}}</span></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron jumbotron-fluid text-sm-center bg-dark">
                    <p><span class="label label-primary">Total for today 0</span></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{ route('admin.category.create') }}">Create category</a>
                @foreach($categories as $category)
                    <a class="list-group-item" href="{{route('admin.category.edit', $category)}}">
                        <h4 class="list-group-item-heading">{!! $category->title !!}</h4>
                        <p class="list-group-item-text">
                            Articles amount: {{ $category->articles()->count() }}
                        </p>
                    </a>
                @endforeach
            </div>
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{ route('admin.article.create') }}">Create article</a>
                @foreach($articles as $article)
                    <a class="list-group-item" href="{{ route('admin.article.edit', $article) }}">
                        <h4 class="list-group-item-heading">{{ $article->title }}</h4>
                        <p class="list-group-item-text">
                           Related categories: {{ $article->categories()->pluck('title')->implode(', ') }}
                        </p>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->

@endsection