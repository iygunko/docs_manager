@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Users')

@section('head_css')
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/extra-libs/multicheck/multicheck.css') }}" rel="stylesheet">
@endsection

@section('title', 'Users')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12 mx-auto">
                <div class="card">
                    @if(session('flash_message_error'))
                        <div class="alert alert-danger alert-block">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! session('flash_message_error') !!}
                        </div>
                    @endif
                    @if(session('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! session('flash_message_success') !!}
                        </div>
                    @endif
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Users</h5>
                    </div>
                    <div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">updated_at</th>
                                <th scope="col">created_at</th>
                                <th scope="col">Roles</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->updated_at }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>
                                    <form action="{{ route('admin.assign') }}" method="post">
                                        User <input type="checkbox" {{ $user->hasRole('User') ? 'checked' : '' }} name="role_user">
                                        Author <input type="checkbox" {{ $user->hasRole('Author') ? 'checked' : '' }} name="role_author">
                                        Admin <input type="checkbox" {{ $user->hasRole('Admin') ? 'checked' : '' }} name="role_admin">

                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{ $user->email }}" name="email">
                                        <button type="submit">Assign Roles</button>
                                    </form>
                                    <td  class="text-left">
                                        <form onsubmit="if(confirm('Delete?')){ return true }else{ return false }" action="{{route('admin.user.destroy', $user)}}" method="post">
                                            <a href="{{ route('admin.user.edit', $user->id) }}" class="btn"><i class="fa fa-edit"></i></a>
                                            <input type="hidden" name="_method" value="DELETE">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-sm"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{ $users->links() }}

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->

@endsection

@section('bottom_js')
    <script src="{{ asset('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/extra-libs/multicheck/datatable-checkbox-init.js') }}"></script>
    <script src="{{ asset('assets/extra-libs/multicheck/jquery.multicheck.js') }}"></script>
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
@endsection
