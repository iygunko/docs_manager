@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Edit User')

@section('title', 'Edit User')

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title') User edit @endslot
        @slot('parent') Homepage @endslot
        @slot('active') User edit @endslot
    @endcomponent

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <form class="form-horizontal" action="{{ route('admin.user.update', $user) }}" method="post">
                    <input type="hidden" name="_method" value="put">
                    {{ csrf_field() }}
                    @include('admin.users.partials.form')

                    <input type="hidden" name="modified_by" value="{{Auth::id()}}">
                </form>
            </div>
        </div>
    </div>


@endsection