@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Create Article')

@section('title', 'Create Article')

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title') Article create @endslot
        @slot('parent') Homepage @endslot
        @slot('active') Article create @endslot
    @endcomponent

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <form class="form-horizontal" action="{{ route('admin.article.store') }}" method="post">
                    {{ csrf_field() }}
                    @include('admin.articles.partials.form')

                    <input type="hidden" name="created_by" value="{{Auth::id()}}">
                </form>
            </div>
        </div>
    </div>


@endsection