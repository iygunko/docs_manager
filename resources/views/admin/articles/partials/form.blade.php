<label for="">Status</label>
<select class="form-control" name="published">
    @if(isset($article->id))
        <option value="0" @if ($article->published == 0) selected="" @endif>Not published</option>
        <option value="1" @if ($article->published == 1) selected="" @endif>Published</option>
    @else
        <option value="0">Not published</option>
        <option value="1">Published</option>
    @endif
</select>

<label for="">Title</label>
<input type="text" class="form-control" name="title" placeholder="Article title" value="{{$article->title or ""}}" required>

<label for="">Slug (Unique value)</label>
<input type="text" class="form-control" name="slug" placeholder="Auto generation" value="{{$article->slug or ""}}" readonly="">

<label for="">Parent category</label>
<select class="form-control" name="categories[]" multiple="">
    @include('admin.articles.partials.categories', ['categories' => $categories])
</select>

<label for="description_short">Short Description</label>
<textarea class="form-control" name="description_short" id="description_short" cols="30" rows="10">{{$article->description_short or ""}}</textarea>

<label for="description">Full Description</label>
<textarea class="form-control" name="description" id="description" cols="30" rows="10">{{$article->description or ""}}</textarea>

<hr />

<label for="">Meta Title</label>
<input type="text" class="form-control" name="meta_title" placeholder="Meta Title" value="{{$article->meta_title or ""}}">

<label for="">Meta Description</label>
<input type="text" class="form-control" name="meta_description" placeholder="Meta Description" value="{{$article->meta_description or ""}}">

<label for="">Keywords</label>
<input type="text" class="form-control" name="meta_keyword" placeholder="Keywords, separated by commas" value="{{$article->meta_keyword or ""}}">


<input class="btn btn-primary" type="submit" value="Save">