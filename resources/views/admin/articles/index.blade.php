@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Articles')

@section('title', 'Articles')

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title') Articles list @endslot
        @slot('parent') Homepage @endslot
        @slot('active') Articles @endslot
    @endcomponent
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <a href="{{ route('admin.article.create') }}" class="btn btn-primary pull-right">
                    <i class="fas fa-plus"></i> Create article</a>
                <table class="table table-striped">
                    <thead>
                    <th>Name</th>
                    <th>Publishing</th>
                    <th class="text-right">Actions</th>
                    </thead>
                    <tbody>
                    @forelse($articles as $article)
                        <tr>
                            <td>{{ $article->title  }}</td>
                            <td>{{ $article->published  }}</td>
                            <td class="text-right">
                                <form onsubmit="if(confirm('Delete?')){ return true }else{ return false }" action="{{route('admin.article.destroy', $article)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}

                                    <a class="btn" href="{{ route('admin.article.edit', $article) }}">
                                        <i class="fa fa-edit"></i></a>

                                    <button type="submit" class="btn"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="text-center"><h2>Data not found</h2></td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3">
                            <ul class="pagination pull-right">
                                {{ $articles->links() }}
                            </ul>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>


@endsection