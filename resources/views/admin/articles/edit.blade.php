@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Edit Article')

@section('title', 'Edit Article')

@section('content')

    @component('admin.components.breadcrumbs')
        @slot('title') Article edit @endslot
        @slot('parent') Homepage @endslot
        @slot('active') Article edit @endslot
    @endcomponent

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <form class="form-horizontal" action="{{ route('admin.article.update', $article) }}" method="post">
                    <input type="hidden" name="_method" value="put">
                    {{ csrf_field() }}
                    @include('admin.articles.partials.form')

                    <input type="hidden" name="modified_by" value="{{Auth::id()}}">
                </form>
            </div>
        </div>
    </div>


@endsection