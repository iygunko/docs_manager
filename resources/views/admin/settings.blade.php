@extends('layouts.adminLayout.admin_design')

@section('meta_title', 'Settings')
@section('title', 'Settings')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    @if(session('flash_message_error'))
                        <div class="alert alert-danger alert-block">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! session('flash_message_error') !!}
                        </div>
                    @endif
                    @if(session('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! session('flash_message_success') !!}
                        </div>
                    @endif
                    <form class="form-horizontal" action="{{ route('admin.settings.post') }}" method="post">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <h4 class="card-title text-center">Change Password</h4>
                            <div class="form-group row">
                                <label for="current_password" class="col-sm-3 text-right control-label col-form-label">Current Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="current_password" id="current_password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="new_password" class="col-sm-3 text-right control-label col-form-label">New password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="new_password" id="new_password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="confirm_password" class="col-sm-3 text-right control-label col-form-label">Confirm password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->

@endsection