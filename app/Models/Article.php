<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'slug', 'description_short', 'description', 'image', 'image_show', 'meta_title',
        'meta_description', 'meta_keyword', 'published', 'created_by', 'modified_by'];

    /**
     * Mutators
     *
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40) . "-" . \Carbon\Carbon::now()->format('dmHi'), '-');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function categories()
    {
        return $this->morphToMany('App\Models\Category', 'categoryable');
    }

    /**
     * @param $query
     * @param $count
     * @return mixed
     */
    public function scopeLastArticles($query, $count)
    {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }
}
