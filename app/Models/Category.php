<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Category
 * @package App\Models
 */
class Category extends Model
{
    /**
     * Mass assigned
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'parent_id', 'published', 'created_by', 'modified_by'];

    /**
     * Mutators
     *
     * @param $value
     */
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40) . "-" . \Carbon\Carbon::now()->format('dmHi'), '-');
    }

    /**
     * Get children category
     */
    public function children()
    {
        return  $this->hasMany(self::class, 'parent_id');
    }

    /**
     * Polymorphic relation with articles
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function articles()
    {
        return $this->morphedByMany('App\Models\Article', 'categoryable');
    }

    /**
     * @param $query
     * @param $count
     * @return mixed
     */
    public function scopeLastCategories($query, $count)
    {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }
}
