<?php

namespace App\Repositories;

use App\Models\Article;
use App\Models\Category;

/**
 * Class CategoryRepository
 * @package App\Articles\Repositories
 */
class CategoryRepository extends BaseRepository
{
    /**
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * @param array $attributes
     * @return bool|mixed
     */
    public function create(array $attributes)
    {
        return $this->model::create($attributes);
    }

    /**
     * @param $limit
     * @param $param
     * @param bool $isDesc
     * @return mixed
     */
    public function getPaginatedOrderBy($limit, $param, $isDesc = true)
    {
        $ordering = ($isDesc) ? 'desc' : 'asc';

        return $this->model::orderBy($param, $ordering)->paginate($limit);
    }

    /**
     * @return Category[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @param array $with
     * @param array $where
     * @return Article[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getWhereWith(array $with, array $where = [])
    {
        return $this->model::with($with)->where($where)->get();
    }

    /**
     * @param array $attributes
     * @param $id
     * @return bool|mixed
     */
    public function update(array $attributes, $id)
    {
        $category = $this->model->find($id);

        return $category->update($attributes);
    }

    /**
     * @param $id
     * @return int|mixed
     */
    public function delete($id)
    {
        $category = $this->model->find($id);

        return $category->delete();
    }
}