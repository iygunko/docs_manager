<?php

namespace App\Repositories;

use App\Models\Article;

/**
 * Class ArticleRepository
 * @package App\Articles\Repositories
 */
class ArticleRepository extends BaseRepository
{
    /**
     * ArticlesRepository constructor.
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->model = $article;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        $article = $this->model::create($attributes);

        if ($attributes['categories']) {
            $article->categories()->attach($attributes['categories']);
        }

        return true;
    }

    /**
     * @param $limit
     * @param $param
     * @param bool $isDesc
     * @return mixed
     */
    public function getPaginatedOrderBy($limit, $param, $isDesc = true)
    {
        $ordering = ($isDesc) ? 'desc' : 'asc';

        return $this->model::orderBy($param, $ordering)->paginate($limit);
    }

    /**
     * @param array $with
     * @param array $where
     * @return Article[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getWhereWith(array $with = [], array $where = [])
    {
        return $this->model::with($with)->where($where)->get();
    }

    /**
     * @return Article[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @param array $attributes
     * @param $id
     * @return bool|mixed
     */
    public function update(array $attributes, $id)
    {
        $this->model->update($attributes);
        $this->model->categories()->detach();
        if ($attributes['categories']) {
            $this->model->categories()->attach($attributes['categories']);
        }

        return true;
    }

    /**
     * @param $id
     * @return bool|int|mixed
     */
    public function delete($id)
    {
        $article = $this->model->find($id);

        $article->categories()->detach();

        $article->delete($id);

        return true;
    }
}