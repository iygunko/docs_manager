<?php
/**
 * Created by PhpStorm.
 * User: komp33beck
 * Date: 28.08.2018
 * Time: 13:57
 */

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    /**
     * Search articles
     *
     * @param  Request $request
     * @return mixed
     */
    public function search(Request $request)
    {

        if($request->has('q')) {

            $articles = Article::with(['categories'])->where('title', 'LIKE', '%'.$request->get('q').'%')->where('published', 1)->paginate(10);
            if($articles->count()){
                foreach($articles as $article){
                    $article->author = User::find($article->created_by)->name;
                }
                return view('user.blog.search',[
                    'articles' => $articles,
                    'q' => $request->get('q')
                ]);
            }
        }

        return view('user.blog.search',['q' => $request->get('q'), 'error' => 'No results found, please try with different keywords.']);

    }

}