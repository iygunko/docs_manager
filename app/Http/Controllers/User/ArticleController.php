<?php

namespace App\Http\Controllers\User;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    public function article($slug)
    {
        $article = Article::where('slug', $slug)->first();

        if(!isset($_COOKIE[$slug])){ // for vew count
            setcookie($slug, true, time()+60*60*24*30);
            $article->viewed += 1;
            $article->timestamps = false;
            $article->save();
        }

        $created_by = User::find($article->created_by)->name;

        return view('user.blog.article', [
            'article' => $article,
            'created_by' => $created_by,
        ]);
    }
}
