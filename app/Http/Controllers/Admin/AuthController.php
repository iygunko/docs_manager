<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
 * Class TestController
 * @package App\Http\Controllers\Admin
 */
class AuthController extends Controller
{

    protected $rules = [
        'name' => 'bail|required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
    ];

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
                return redirect()->route('admin.dashboard');
            } else {
                return Redirect::back()->with('flash_message_error', 'Invalid Username or Password');
            }
        }

        return view('admin.admin_login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function register(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            $this->validate($request, $this->rules);
            $user = new User();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->save();
            $user->roles()->attach(Role::where('name', 'User')->first());
            return redirect()->route('login')->with('flash_message_success', 'You are successfully registered');
        }

        return view('admin.admin_register');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Session::flush();

        return Redirect::route('login')->with('flash_message_success', 'Logged out Successfully');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings()
    {
        return view('admin.settings');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangePassword(Request $request)
    {
        $data = $request->all();
        //@TODO check new and confirm passwords

        $requestedUser = User::where(['email' => Auth::user()->email])->first();
        $currentPassword = $data['current_password'];

        if (Hash::check($currentPassword, $requestedUser->password)) {
            $newEncyptedPassword = bcrypt($data['new_password']);
            User::where('id', Auth::user()->id)->update(['password' => $newEncyptedPassword]);

            return redirect()->route('admin.settings')->with(['flash_message_success' => 'Password is successfully updated']);
        } else {
            return redirect()->route('admin.settings')->with(['flash_message_error' => 'Incorrect current password']);
        }
    }
}
