<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    const USERS_PER_PAGE = 5;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(self::USERS_PER_PAGE);

        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::find($id);
        $result = $user->update(['name' => $data['name'], 'email' => $data['email']]);

        if ($result) {
            return redirect()->route('admin.user.index')->with('flash_message_success', 'User info was updated.');
        } else {
            return redirect()->route('admin.user.index')->with('flash_message_success', 'User info was not updated. Some error occurred.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user->delete()) {
            return redirect()->back()->with('flash_message_success', 'User was deleted.');
        } else {
            return redirect()->back()->with('flash_message_error', 'User was not deleted. Some error occurred.');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdminAssignRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->first();

        if (isset($request['role_user'])) {
            $user->roles()->attach(Role::where('name', 'User')->first());
        } else {
            $user->roles()->detach(Role::where('name', 'User')->first());
        }
        if (isset($request['role_author'])) {
            $user->roles()->attach(Role::where('name', 'Author')->first());
        } else {
            $user->roles()->detach(Role::where('name', 'Author')->first());
        }
        if (isset($request['role_admin'])) {
            $user->roles()->attach(Role::where('name', 'Admin')->first());
        } else {
            $user->roles()->detach(Role::where('name', 'Admin')->first());
        }

        return redirect()->back()->with('flash_message_success', 'Roles were successfully updated');
    }
}
