<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/**
 * Class TestController
 * @package App\Http\Controllers\Admin
 */
class SettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings()
    {
        return view('admin.settings');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangePassword(Request $request)
    {
        $data = $request->all();
        //@TODO check new and confirm passwords

        $requestedUser = User::where(['email' => Auth::user()->email])->first();
        $currentPassword = $data['current_password'];

        if (Hash::check($currentPassword, $requestedUser->password)) {
            $newEncyptedPassword = bcrypt($data['new_password']);
            User::where('id', Auth::user()->id)->update(['password' => $newEncyptedPassword]);

            return redirect()->route('admin.settings')->with(['flash_message_success' => 'Password is successfully updated']);
        } else {
            return redirect()->route('admin.settings')->with(['flash_message_error' => 'Incorrect current password']);
        }
    }
}
