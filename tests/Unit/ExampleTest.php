<?php

namespace Tests\Unit;

use App\Models\Article;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        //mock object creating
        $authorizeNet = $this->getMockBuilder('App\Repositories\ArticleRepository')
            ->setConstructorArgs( [new Article()])
            ->setMethods(['getAll', 'create'])
            ->getMock();

        $response = new \stdClass();
        $response->approved = true;
        $response->transaction_id = 123;

        //set return value for any method inside the mock object
        $authorizeNet->expects($this->any())//number of times we are expecting the method to be called
            ->method('create')
            ->will($this->returnValue($response));

        $this->assertTrue(1 == 1);
    }
}