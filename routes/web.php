<?php
Route::match(['get', 'post'], '/login', 'Admin\AuthController@login')->name('login');
Route::match(['get', 'post'], '/register', 'Admin\AuthController@register')->name('register');

Route::group(['prefix' => 'admin', 'middleware' => 'roles', 'roles' => ['Admin', 'Author', 'User']], function () {
    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');
    Route::get('settings', 'Admin\SettingsController@settings')->name('admin.settings');
    Route::post('settings-post', 'Admin\SettingsController@postChangePassword')->name('admin.settings.post');

    Route::get('blog/category/{slug?}', 'User\CategoryController@category')->name('category');
    Route::get('blog/article/{slug?}', 'User\ArticleController@article')->name('article');

    Route::get('blog/search', 'User\SearchController@search')->name('admin.search');
});

Route::group(['prefix' => 'admin', 'middleware' => 'roles', 'roles' => ['Admin', 'Author']], function () {
    Route::resource('category', 'Admin\CategoryController', ['as' => 'admin']);
    Route::resource('article', 'Admin\ArticleController', ['as' => 'admin']);
});

Route::group(['prefix' => 'admin', 'middleware' => 'roles', 'roles' => ['Admin']], function () {
    Route::resource('user', 'Admin\UserController', ['as' => 'admin']);

    Route::post('users/update-roles', 'Admin\UserController@postAdminAssignRoles')->name('admin.assign');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('logout', 'Admin\AuthController@logout')->name('logout');
    Route::get('/', function () {
        return view('welcome');
    });
});